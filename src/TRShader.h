//
// Created by foil on 19/07/17.
//

#ifndef FIESTA_TRSHADER_H
#define FIESTA_TRSHADER_H

#include "Shader.h"
#include "Uniform/Uniform1f.h"
#include "Uniform/Uniform2f.h"
#include "Uniform/Uniform2i.h"
#include "Uniform/UniformMatrix4fv.h"

class TRShader : public Shader {
public:
    TRShader(){};
    ~TRShader(){};

    void setUniform1f(string uniformName, float *f1);
    void setUniform2f(string uniformName, float *f1, float *f2);
    void setUniform2i(string uniformName, int *i1, int *i2);
    void setUniformMat4fv(string uniformName, glm::mat4*);

    void use();
};


#endif //FIESTA_TRSHADER_H
