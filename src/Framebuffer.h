//
// Created by foil on 27/07/17.
//

#ifndef SIESTA_FRAMEBUFFER_H
#define SIESTA_FRAMEBUFFER_H

#include <GL/glew.h>
#include <vector>
#include <iostream>

class Framebuffer {

private:
    unsigned int id;                   //framebuffer object
    unsigned int texture;

    //generate an empty color texture with 4 channels (RGBA8) using bilinear filtering
    void generateColorTexture(unsigned int width, unsigned int height);

    //generate an empty depth texture with 1 depth channel using bilinear filtering
    void generateDepthTexture(unsigned int width, unsigned int height);

    void generateFBO(unsigned int width, unsigned int height, unsigned int type);

public:
    unsigned int width, height;
    unsigned int type; // TODO: not cool, rewrite


    Framebuffer();
    ~Framebuffer();

    void destroy();

    unsigned int getId();
    void init(unsigned int width, unsigned int height, unsigned int type);
    void resize(unsigned int width, unsigned int height);
    unsigned int getTexture();
    void bind();
    void unbind();


};


#endif //SIESTA_FRAMEBUFFER_H
