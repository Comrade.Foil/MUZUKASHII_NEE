//
// Created by foil on 07/08/17.
//

#ifndef MUZUKASHII_NEE_TRANSFORMS_H
#define MUZUKASHII_NEE_TRANSFORMS_H


#include <glm/vec4.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glm/gtx/quaternion.hpp"

#include <GL/glew.h>

using namespace glm;

class Transforms {
protected:
    vec4 pos;
    mat4 scale;
    mat4 rot;

    glm::quat quat;
    vec3 axis;
    float angle;

    mat4 bakedData;

public:
    Transforms();
    ~Transforms();

    void setPosition(float x, float y, float z, float w);
    void setPosition(vec4 position);

    void setScale(float scaleX, float scaleY, float scaleZ);
    void setScale(float scale);
    void setScale(mat4 matrix);

    void setQuat(float angle, float y, float z, float w);
    void setQuat(float angle, vec3 axis);
    void setQuatAngle(float angle);
    void setQuatAxis(float y, float z, float w);
    void setQuatAxis(vec3 axis);

    vec4 getPosition();
    mat4 getScale();
    float getQuatAngle();
    vec3 getQuatAxis();
    mat4 getRotationMatrix();

    // TODO: think about separate baking system for different variables (pos+scale) or (scale+rot)
    void bakeUp();
    void calculateRotation();

};


#endif //MUZUKASHII_NEE_TRANSFORMS_H
