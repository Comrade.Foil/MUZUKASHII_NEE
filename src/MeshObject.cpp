//
// Created by foil on 10/08/17.
//

#include "MeshObject.h"

MeshObject::MeshObject(MeshData *meshData) {
	this->data = meshData;

    glGenVertexArrays(1, &this->vao);

    this->verticesBuffer = new Buffer(GL_ARRAY_BUFFER);
    this->normalsBuffer = new Buffer(GL_ARRAY_BUFFER);
    this->texcoordsBuffer = new Buffer(GL_ARRAY_BUFFER);

	verticesNumber = this->data->getVerticesNum();

    setupBuffers();
}

MeshObject::~MeshObject() {
    glDeleteVertexArrays(1, &vao);

    delete data;
    delete verticesBuffer;
    delete normalsBuffer;
    delete texcoordsBuffer;

    data       = 0;
    verticesBuffer   = 0;
    normalsBuffer    = 0;
    texcoordsBuffer  = 0;
}

void MeshObject::setupBuffers() {
    glBindVertexArray(this->vao);

        verticesBuffer->setupData(data->vertexData.data(), data->vertexData.size(), sizeof(GLfloat), GL_STATIC_DRAW);
        verticesBuffer->setupVertexAttribPointer(0, 3, GL_FLOAT, 3*sizeof(float), 0);

        normalsBuffer->setupData(data->normalsData.data(), data->normalsData.size(), sizeof(GLfloat), GL_STATIC_DRAW);
        normalsBuffer->setupVertexAttribPointer(1, 3, GL_FLOAT, 3*sizeof(float), 0);

        texcoordsBuffer->setupData(data->texcoordData.data(), data->texcoordData.size(), sizeof(GLfloat), GL_STATIC_DRAW);
        texcoordsBuffer->setupVertexAttribPointer(2, 2, GL_FLOAT, 2*sizeof(float), 0);

    glBindVertexArray(0);
}
