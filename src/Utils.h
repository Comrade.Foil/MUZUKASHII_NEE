//
// Created by foil on 25/11/16.
//

#ifndef FIESTA_UTILS_H
#define FIESTA_UTILS_H

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <string>
#include <iostream>
#include <GL/glew.h>

using namespace glm;

class Vertex {
public:
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoord;


    Vertex() {}

    Vertex(glm::vec3 pos, glm::vec3 normal, glm::vec2 tex) {
        this->position = pos;
        this->texCoord = tex;
        this->normal = normal;
    }

    Vertex(glm::vec3 pos, glm::vec3 normal) {
        this->position = pos;
        this->normal = normal;
    }

    Vertex(glm::vec3 pos) {
        this->position = pos;
    }

    ~Vertex() {
        position.~vec3();
        texCoord.~vec2();
        normal.~vec3();
    }

    void setPosition(glm::vec3 pos) {
        this->position = pos;
    }

    void setNormal(glm::vec3 norm) {
        this->normal = norm;
    }

    void setTexCoord(glm::vec2 tex) {
        this->texCoord = tex;
    }
};

class Texture {
public:
    GLuint id;
    std::string type;
    std::string path;
};

struct DirLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

#endif //FIESTA_UTILS_H

