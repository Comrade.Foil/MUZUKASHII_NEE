//
// Created by foil on 25/11/16.
//

#ifndef FIESTA_ENTITY_H
#define FIESTA_ENTITY_H


class Entity {
public:
    Entity(){};
    virtual ~Entity(){};

    virtual void update(double) = 0;
};


#endif //FIESTA_ENTITY_H
