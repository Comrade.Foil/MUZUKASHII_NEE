//
// Created by foil on 07/08/17.
//

#include "Transforms.h"

Transforms::Transforms() {
    this->pos       = vec4(0.0);
    this->scale     = mat4(1.0, 0.0, 0.0, 0.0,
                           0.0, 1.0, 0.0, 0.0,
                           0.0, 0.0, 1.0, 0.0,
                           0.0, 0.0, 0.0, 1.0);

    this->rot       = mat4(1.0);
    this->axis      = vec3(0.0, 0.0, 0.0);
    this->angle     = 0.0f;
    this->bakedData = mat4(0.0);

    calculateRotation();
}

Transforms::~Transforms() {

}

void Transforms::setPosition(float x, float y, float z, float w) {
    this->pos = vec4(x, y, z, w);
}

void Transforms::setPosition(vec4 newPos) {
    this->pos = newPos;
}

void Transforms::setScale(float scaleX, float scaleY, float scaleZ) {
    this->scale = mat4(scaleX, 0.0, 0.0, 0.0,
                       0.0, scaleY, 0.0, 0.0,
                       0.0, 0.0, scaleZ, 0.0,
                       0.0,  0.0,  0.0,  1.0);
}

void Transforms::setScale(float scale) {
    this->scale = mat4(scale, 0.0, 0.0, 0.0,
                       0.0, scale, 0.0, 0.0,
                       0.0, 0.0, scale, 0.0,
                       0.0,  0.0,  0.0, 1.0);
}

void Transforms::setScale(mat4 newScale) {
    this->scale = newScale;
}

void Transforms::setQuat(float angle, float x, float y, float z) {
    this->angle = angle;
    this->axis = vec3(x, y, z);

    calculateRotation();
}

void Transforms::setQuat(float angle, vec3 axis) {
    this->angle = angle;
    this->axis = axis;

    calculateRotation();
}

void Transforms::setQuatAngle(float angle) {
    this->angle = angle;

    calculateRotation();
}

void Transforms::setQuatAxis(float x, float y, float z) {
    this->axis = vec3(x, y, z);

    calculateRotation();
}

void Transforms::setQuatAxis(vec3 axis) {
    this->axis = axis;

    calculateRotation();
}


void Transforms::calculateRotation() {
    this->quat = angleAxis(this->angle, this->axis);
    this->rot  = toMat4(this->quat);
}

vec4 Transforms::getPosition() {
    return this->pos;
}

mat4 Transforms::getScale() {
    return this->scale;
}

float Transforms::getQuatAngle() {
    return this->angle;
}

vec3 Transforms::getQuatAxis() {
    return this->axis;
}

mat4 Transforms::getRotationMatrix() {
    return this->rot;
}



