//
// Created by foil on 12/12/16.
//

#ifndef FIESTA_SHADERMANAGER_H
#define FIESTA_SHADERMANAGER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <unordered_map>
#include "Shader.h"

using namespace std;


class ShaderManager {
public:
    string ROOT_FOLDER_PATH;
    unordered_map<string, Shader*> shaderMap;

    ShaderManager(string rootPath);
    ~ShaderManager();

    void addShader(string name, const string vertexPath, const string fragmentPath);
    void addShader(string name, const string vertexPath, const string fragmentPath, const string geometryPath);
    void addShader(Shader* shader);
    void addShader(string name, Shader* shader);

    void useShader(string name);

    Shader* getShader(string name);
    GLuint getShaderId(string name);

};


#endif //FIESTA_SHADERMANAGER_H
