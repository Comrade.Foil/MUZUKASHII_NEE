//
// Created by stefan on 3/4/18.
//

#include "GameObject.h"

GameObject::GameObject(const char* modelName, const char* textureName, vec3 pos) {
	this->modelName = modelName;
	this->textureName = textureName;

	this->meshObject = new MeshObject(g_storage->getMesh(modelName));
	this->transforms = Transforms();
	this->transforms.setPosition(pos.x, pos.y, pos.z, 1.0f);
}

GameObject::~GameObject() {

};

void GameObject::render(Shader* program) {
	program->use();
	sendUniformData(program->id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(
			GL_TEXTURE_2D,
			g_storage->getTexture(this->textureName)
	);
	glBindVertexArray(meshObject->vao);
	glDrawArrays(GL_TRIANGLES, 0, meshObject->verticesNumber);

}

void GameObject::update(double time) {

}

void GameObject::sendUniformData(GLuint programID) {
	vec4 position = this->transforms.getPosition();
	mat4 rotation = this->transforms.getRotationMatrix();
	mat4 scaling = this->transforms.getScale();

	glUniform3f(
			glGetUniformLocation(programID, "uPos"),
			position.x,
			position.y,
			position.z
	);
	glUniformMatrix4fv(
			glGetUniformLocation(programID, "uRot"),
			1,
			GL_FALSE,
			glm::value_ptr(rotation)
	);
	glUniformMatrix4fv(
			glGetUniformLocation(programID, "uScale"),
			1,
			GL_FALSE,
			glm::value_ptr(scaling)
	);
}

