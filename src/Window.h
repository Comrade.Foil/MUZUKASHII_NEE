//
// Created by foil on 26/07/17.
//

#ifndef SIESTA_WINDOW_H
#define SIESTA_WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec4.hpp>

#include <string>

using namespace std;

class Window {
private:
    GLFWwindow* windowHandle;
    glm::vec4 color = glm::vec4(0.0);


public:
    Window();
    Window(unsigned int, unsigned int, const char* name);
    ~Window();

    static void resize(GLFWwindow*, int, int);
    void clearColor(float, float, float, float);
    bool shouldClose();
    void clearColor();
    void swapBuffers();

    glm::ivec2 getFramebufferSize();
    glm::ivec2 getWindowSize();
    // TODO: part-time solution
    GLFWwindow* getWindowHandle();

};


#endif //SIESTA_WINDOW_H
