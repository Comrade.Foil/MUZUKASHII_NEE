//
// Created by stefan on 3/4/18.
//

#ifndef MUZUKASHII_NEE_GAMEOBJECT_H
#define MUZUKASHII_NEE_GAMEOBJECT_H


#include "Renderable.h"
#include "Transforms.h"
#include "MeshObject.h"
#include "global.h"


class GameObject : public Renderable{
public:
	Transforms transforms;
	MeshObject* meshObject;

	string modelName, textureName;

	GameObject(const char* modelName, const char* textureName, vec3 pos);
	~GameObject();

	void update(double time);
	void render(Shader* program);

private:
	void sendUniformData(GLuint programID);
};


#endif //MUZUKASHII_NEE_GAMEOBJECT_H
