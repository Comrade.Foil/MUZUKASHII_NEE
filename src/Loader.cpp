//
// Created by foil on 10/07/17.
//

#include "Loader.h"



Loader::Loader(std::string modelPath, std::string texturesPath) {
    this->MODEL_FOLDER_PATH = modelPath;
    this->TEXTURE_FOLDER_PATH = texturesPath;
}

void Loader::processNode(aiNode *node, const aiScene *scene, MeshData* meshData) {
    // process all the node's meshes (if any)
    for(unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh *aiMesh = scene->mMeshes[node->mMeshes[i]];
        processMesh(aiMesh, scene, meshData);
    }
    // then do the same for each of its children
    for(unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene, meshData);
    }
}

void Loader::processMesh(aiMesh *mesh, const aiScene *scene, MeshData* meshData) {
    vector<unsigned int> indices;
    for(unsigned int i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for(unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }
    // Walk through each of the mesh's vertices
    for(unsigned int i = 0; i < indices.size(); i++) {
        unsigned int index = indices[i];
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // positions
        vector.x = mesh->mVertices[index].x;
        vector.y = mesh->mVertices[index].y;
        vector.z = mesh->mVertices[index].z;

        meshData->pushVertex(vector);

        // normals
        vector.x = mesh->mNormals[index].x;
        vector.y = mesh->mNormals[index].y;
        vector.z = mesh->mNormals[index].z;

        meshData->pushNormal(vector);

        // texture coordinates
        if(mesh->mTextureCoords[0]) {// does the mesh contain texture coordinates?{
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][index].x;
            vec.y = mesh->mTextureCoords[0][index].y;

            meshData->pushTexcoord(vec);
        }
    }



}

bool Loader::assimpLoadOBJ(std::string path, std::string modelName) {
    Assimp::Importer importer;
    path = this->MODEL_FOLDER_PATH + path;
    const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate|aiProcess_FlipUVs|aiProcess_CalcTangentSpace);
    MeshData* meshData = new MeshData();

    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        cout << "ERROR::ASSIMP::" << importer.GetErrorString() << endl;
        return false;
    }
    processNode(scene->mRootNode, scene, meshData);

    g_storage->storeData(modelName, meshData);
    return true;
}

void Loader::loadTexture(string imageName) {
    string filePath = TEXTURE_FOLDER_PATH + imageName;

    printf("Reading image %s\n", (TEXTURE_FOLDER_PATH + imageName).c_str());

    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(filePath.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << filePath << std::endl;
        stbi_image_free(data);
    }

    g_storage->storeTexture(imageName, textureID);
}

Loader::~Loader() {

}



