//
// Created by foil on 26/12/16.
//

#ifndef FIESTA_CAMERA_H
#define FIESTA_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/vec3.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "global.h"
#include "Renderable.h"

using namespace glm;

class Camera : public Entity {
private:
    ivec2 windowSize;

    mat4 viewMatrix, modelMatrix, projMatrix, MVP;
    GLfloat camSpd, mouseSpd;
    GLdouble cursorStartX, cursorStartY, horAngle, vertAngle;
    bool RMB_DOWN;

    GLFWwindow* windowHandle;
public:
    Camera(vec3 pos, GLFWwindow *window);
    ~Camera();

    mat4* getMVP();
    vec3 position;
    void update(GLdouble dTime);
    void render(GLuint);
};


#endif //FIESTA_CAMERA_H
