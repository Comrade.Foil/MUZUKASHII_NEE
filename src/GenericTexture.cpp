//
// Created by foil on 27/07/17.
//

#include "GenericTexture.h"

GenericTexture::GenericTexture(GLuint width, GLuint height, GLuint type, Shader* shader) {
    attachedShader = shader;
    fbo.init(width, height, type);
}

GenericTexture::~GenericTexture() {
    attachedShader = 0;
    fbo.destroy();
}

unsigned int GenericTexture::getId() {
    return this->fbo.getId();
}


void GenericTexture::attachShader(Shader* shader) {
    this->attachedShader = shader;
}

GLuint GenericTexture::getType() {
    return fbo.type;
}

void GenericTexture::update() {
    glViewport(0, 0, fbo.width, fbo.width);
    fbo.bind();
    attachedShader->use();
    glDrawArrays(GL_TRIANGLES, 0, 3);
    fbo.unbind();
}

unsigned int GenericTexture::getTexture() {
    return fbo.getTexture();
}

void GenericTexture::bindFBO() {
    this->fbo.bind();
}

void GenericTexture::unbindFBO() {
    this->fbo.unbind();
}

GenericTexture::GenericTexture(GLuint width, GLuint height, GLuint type) {
    fbo.init(width, height, type);
}




