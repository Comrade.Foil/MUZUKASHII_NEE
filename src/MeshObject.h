//
// Created by foil on 10/08/17.
//

#ifndef MUZUKASHII_NEE_BLANKOBJECT_H
#define MUZUKASHII_NEE_BLANKOBJECT_H


#include <GL/glew.h>
#include "Renderable.h"
#include "MeshData.h"
#include "Transforms.h"
#include "Buffer.h"

class MeshObject {
public:
	MeshObject(MeshData *meshData);
	
	~MeshObject();
	
	MeshData *data;
	Buffer *verticesBuffer, *normalsBuffer, *texcoordsBuffer;
	GLuint vao;
	GLint verticesNumber;

private:
	void setupBuffers();
};


#endif //MUZUKASHII_NEE_BLANKOBJECT_H
