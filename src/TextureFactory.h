//
// Created by foil on 27/07/17.
//

#ifndef SIESTA_TEXTUREFACTORY_H
#define SIESTA_TEXTUREFACTORY_H


#include "GenericTexture.h"
#include "global.h"

class TextureFactory {
private:
    vector<GenericTexture*> dynamicTextures;
    vector<GenericTexture*> staticTextures;

    unordered_map<string, GenericTexture*> map;

public:
    TextureFactory();
    ~TextureFactory();

    void createStaticTexture(GLuint width, GLuint height, GLuint type, Shader* shader);
    void createDynamicTexture(GLuint width, GLuint height, GLuint type, Shader* shader);

    void createStaticTexture(GLuint width, GLuint height, GLuint type, string name);
    void createDynamicTexture(GLuint width, GLuint height, GLuint type, string name);

    void putStaticTexture(GenericTexture*, string name);
    void putDynamicTexture(GenericTexture*, string name);

    void updateDynamicTextures();
    void updateStaticTextures();

    void updateStaticTexture(string name);
    void updateDynamicTexture(string name);

    void bindSpecificFBO(string name);
    void unbindSpecificFBO(string name);


};


#endif //SIESTA_TEXTUREFACTORY_H
