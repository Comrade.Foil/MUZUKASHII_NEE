//
// Created by foil on 27/07/17.
//

#include "Framebuffer.h"


Framebuffer::Framebuffer() {

}

void Framebuffer::init(unsigned int width, unsigned int height, unsigned int type)  {
    this->type = type;
    this->width = width;
    this->height = height;

    generateFBO(width, height, type);

}

Framebuffer::~Framebuffer() {
    destroy();
}

void Framebuffer::destroy() {
    glDeleteFramebuffers(1, &id);
    glDeleteTextures(1, &texture);
}

void Framebuffer::generateColorTexture(unsigned int width, unsigned int height) {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
}

void Framebuffer::generateDepthTexture(unsigned int width, unsigned int height) {

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

}

// GL_COLOR_ATTACHMENT0 or GL_DEPTH_ATTACHMENT

void Framebuffer::generateFBO(unsigned int width, unsigned int height, unsigned int type) {

    //Generate a framebuffer object(FBO) and bind it to the pipeline
    glGenFramebuffers(1, &id);
    glBindFramebuffer(GL_FRAMEBUFFER, id);

    if (type == GL_COLOR_ATTACHMENT0) {
        generateColorTexture(width, height); //generate empty texture
    } else if (type == GL_DEPTH_ATTACHMENT) {
        generateDepthTexture(width, height); //generate empty texture
    }

    glFramebufferTexture(GL_FRAMEBUFFER, type, texture, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
        std::cout << "Error! FrameBuffer is not complete" << std::endl;
        std::cin.get();
        std::terminate();
    }

    //unbind framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

unsigned int Framebuffer::getTexture() {
    return texture;
}

void Framebuffer::resize(unsigned int width, unsigned int height) {
    destroy();
    init(width, height, this->type);
}

void Framebuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void Framebuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

unsigned int Framebuffer::getId() {
    return this->id;
}

