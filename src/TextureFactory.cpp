//
// Created by foil on 27/07/17.
//

#include "TextureFactory.h"

TextureFactory::TextureFactory() {

}

TextureFactory::~TextureFactory() {
    cout << "some TextureFactory destructor" << endl;


    for(unordered_map<string, GenericTexture*>::iterator i = map.begin(); i != map.end(); ++i) {
        delete i->second; // go through map and destroy members
    }

    vector<GenericTexture*>().swap(dynamicTextures);
    vector<GenericTexture*>().swap(staticTextures);
}

void TextureFactory::createStaticTexture(GLuint width, GLuint height, GLuint type, Shader* shader) {
    GenericTexture* gt = new GenericTexture(width, height, type, shader);

    staticTextures.push_back(gt);
    map.insert(make_pair(shader->name, gt));

    g_storage->storeTexture(shader->name, gt->getTexture());
}

void TextureFactory::createStaticTexture(GLuint width, GLuint height, GLuint type, string name) {
    GenericTexture* gt = new GenericTexture(width, height, type);
    map.insert(make_pair(name, gt));
    g_storage->storeTexture(name, gt->getTexture());
}

void TextureFactory::createDynamicTexture(GLuint width, GLuint height, GLuint type, Shader* shader) {
    GenericTexture* gt = new GenericTexture(width, height, type, shader);

    dynamicTextures.push_back(gt);
    map.insert(make_pair(shader->name, gt));

    g_storage->storeTexture(shader->name, gt->getTexture());
}

void TextureFactory::createDynamicTexture(GLuint width, GLuint height, GLuint type, string name) {
    GenericTexture* gt = new GenericTexture(width, height, type);
    map.insert(make_pair(name, gt));
    g_storage->storeTexture(name, gt->getTexture());
}

void TextureFactory::putStaticTexture(GenericTexture* gt, string name) {
    staticTextures.push_back(gt);
    map.insert(make_pair(name, gt));

    g_storage->storeTexture(name, gt->getTexture());
}

void TextureFactory::putDynamicTexture(GenericTexture* gt, string name) {
    dynamicTextures.push_back(gt);
    map.insert(make_pair(name, gt));

    g_storage->storeTexture(name, gt->getTexture());
}

void TextureFactory::updateDynamicTextures() {
    for (int i=0; i < dynamicTextures.size(); i++) {
        dynamicTextures[i]->update();
    }
}

void TextureFactory::updateStaticTextures() {
    for (int i=0; i < staticTextures.size(); i++) {
        staticTextures[i]->update();
    }
}

void TextureFactory::updateStaticTexture(string name) {
    if (map.find(name) == map.end()) {
        cout << "(TextureFactory) Error: can't updateStaticTexture " << name << "is not found in map" << endl;
    } else {
        map[name]->update();
    }
}

void TextureFactory::updateDynamicTexture(string name) {
    if (map.find(name) == map.end()) {
        cout << "(TextureFactory) Error: can't updateStaticTexture " << name << "is not found in map" << endl;
    } else {
        map[name]->update();
    }
}

void TextureFactory::bindSpecificFBO(string name) {
    if (map.find(name) == map.end()) {
        cout << "(TextureFactory) Error: can't bindSpecificFBO " << name << "is not found in map" << endl;
    } else {
        map[name]->bindFBO();
    }
}

void TextureFactory::unbindSpecificFBO(string name) {
    if (map.find(name) == map.end()) {
        cout << "(TextureFactory) Error: can't unbindSpecificFBO " << name << "is not found in map" << endl;
    } else {
        map[name]->unbindFBO();
    }
}
