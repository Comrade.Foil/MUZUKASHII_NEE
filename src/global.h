//
// Created by foil on 28/07/17.
//

#ifndef MUZUKASHII_NEE_GLOBAL_H
#define MUZUKASHII_NEE_GLOBAL_H

#include "Storage.h"
#include "Window.h"

extern Storage*        g_storage;
extern Window*         g_window;


#endif //MUZUKASHII_NEE_GLOBAL_H
