//
// Created by foil on 12/12/16.
//

#ifndef FIESTA_SHADER_H
#define FIESTA_SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>
#include <vector>
#include <unordered_map>
#include "Uniform/Uniform.h"


using namespace std;


class Shader {
public:
    GLuint id;
    string name;
    unordered_map<string, GLint> uniformMap;
    vector<Uniform*> uniforms;

    Shader();
    ~Shader();

    void create(string name, string vShaderSource, string fShaderSource, string gShaderSource);
    void create(string name, string vShaderSource, string fShaderSource);

    virtual void use();

protected:
    GLuint createProgram(GLuint &vertexID, GLuint &fragmentID);
    GLuint createProgram(GLuint &vertex, GLuint &fragment, GLuint &geometry);

    string loadShader(const string shaderPath);
};


#endif //FIESTA_SHADER_H
