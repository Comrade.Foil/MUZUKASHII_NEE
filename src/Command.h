//
// Created by stefan on 3/2/18.
//

#ifndef MUZUKASHII_NEE_COMMAND_H
#define MUZUKASHII_NEE_COMMAND_H


class Command {
public:
	virtual ~Command() {}
	virtual void execute(GameObject& mesh) = 0;
};


#endif //MUZUKASHII_NEE_COMMAND_H
