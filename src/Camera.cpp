//
// Created by foil on 26/12/16.
//

#include <iostream>
#include "Camera.h"

Camera::Camera(vec3 pos, GLFWwindow *window) {
    position = pos;
    windowHandle = window;

    mouseSpd  = 0.0009f;
    camSpd    = 0.0001f;
    horAngle  = 0.0f;
    vertAngle = 0.0f;

    glfwGetWindowSize(window, &windowSize.x, &windowSize.y);

    cursorStartX = windowSize.x/2;
    cursorStartY = windowSize.y/2;
    RMB_DOWN = false;
}

Camera::~Camera() {

}

mat4* Camera::getMVP() {
    return &MVP;
}

void Camera::render(GLuint programId) {
    glUniformMatrix4fv(glGetUniformLocation(programId, "MVP"), 1, GL_FALSE, &this->MVP[0][0]);

}

void Camera::update(GLdouble dTime) {
    double xpos = 0, ypos = 0;

    if( glfwGetMouseButton( this->windowHandle, GLFW_MOUSE_BUTTON_RIGHT ) == GLFW_PRESS ){
        if(!RMB_DOWN){
            glfwGetCursorPos(this->windowHandle, &cursorStartX, &cursorStartY);
            RMB_DOWN = true;
        }
    }else{
        RMB_DOWN = false;
    }

    if(RMB_DOWN){
        glfwGetCursorPos(this->windowHandle, &xpos, &ypos);

        horAngle    += mouseSpd * double( cursorStartX - xpos );
        vertAngle   += mouseSpd * double( cursorStartY - ypos );

        if (vertAngle > 3.14f/2.0f) vertAngle = 3.14f/2.0f;
        if (vertAngle < -3.14f/2.0f) vertAngle = -3.14f/2.0f;

        glfwSetCursorPos(this->windowHandle, cursorStartX, cursorStartY);
    }

    glm::vec3 direction(
            cos(vertAngle) * sin(horAngle),
            sin(vertAngle),
            cos(vertAngle) * cos(horAngle)
    );

    direction = glm::normalize(direction);

    // Right vector
    glm::vec3 right = glm::vec3(
            sin(horAngle - 3.14f/2.0f),
            0,
            cos(horAngle - 3.14f/2.0f)
    );
    right = glm::normalize(right);

    // Up vector
    glm::vec3 up = glm::cross(right, direction);
    up = glm::normalize(up);


    if (glfwGetKey( this->windowHandle, GLFW_KEY_W ) == GLFW_PRESS
		|| (glfwGetMouseButton( this->windowHandle, GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS
			&& glfwGetMouseButton( this->windowHandle, GLFW_MOUSE_BUTTON_RIGHT ) == GLFW_PRESS )){
        position += direction * (camSpd + (GLfloat)dTime);
    }

    if (glfwGetKey( this->windowHandle, GLFW_KEY_S ) == GLFW_PRESS){
        position -= direction * (camSpd + (GLfloat)dTime);

    }

    if (glfwGetKey( this->windowHandle, GLFW_KEY_D ) == GLFW_PRESS){
        position += right * (camSpd + (GLfloat)dTime);
    }

    if (glfwGetKey( this->windowHandle, GLFW_KEY_A ) == GLFW_PRESS){
        position -= right * (camSpd + (GLfloat)dTime);
    }

    if (glfwGetKey( this->windowHandle, GLFW_KEY_E ) == GLFW_PRESS){
        position += vec3(0.0f, 1.0f, 0.0f) * (camSpd + (GLfloat)dTime);
    }

    if (glfwGetKey( this->windowHandle, GLFW_KEY_Q ) == GLFW_PRESS){
        position += vec3(0.0f, -1.0f, 0.0f) * (camSpd + (GLfloat)dTime);
    }

    projMatrix = perspective(45.0f, (float)windowSize.x/(float)windowSize.y, 0.001f, 1500.0f);

    viewMatrix = lookAt(
            position,                // Camera is here
            position+direction,      // and looks here : at the same position, plus "direction"
            up                       // Head is up (set to 0,-1,ue to look upside-down)
    );

    modelMatrix = mat4(1.0f);

    MVP = projMatrix * viewMatrix * modelMatrix;
}
