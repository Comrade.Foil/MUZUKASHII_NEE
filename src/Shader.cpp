//
// Created by foil on 12/12/16.
//

#include "Shader.h"

Shader::Shader() {

}

void Shader::create(string name, string vShaderPath, string fShaderPath, string gShaderPath) {
    this->name = name;

    const char *vertexSource = loadShader(vShaderPath).c_str(),
               *fragmentSource = loadShader(fShaderPath).c_str(),
               *geometrySource = loadShader(gShaderPath).c_str();

    GLuint vertex, fragment, geometry;
    GLint success;
    GLchar infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexSource, NULL);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog
        << std::endl;
    };

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentSource, NULL);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog
        << std::endl;
    };

    geometry = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometry, 1, &geometrySource, NULL);
    glCompileShader(geometry);

    glGetShaderiv(geometry, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(geometry, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::GEOMETRY::COMPILATION_FAILED\n" << infoLog
                  << std::endl;
    };

    this->id = createProgram(vertex, fragment, geometry);

    if (this->id == 0) {
        std::cout << "FAILED SHADER: " << name << std::endl;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
    glDeleteShader(geometry);
}

void Shader::create(string name, string vShaderPath, string fShaderPath) {
    this->name = name;

    string vSource = loadShader(vShaderPath),
           fSource =  loadShader(fShaderPath);

    const char *vertexSource   = vSource.c_str(),
               *fragmentSource = fSource.c_str();

    GLuint vertex, fragment;
    GLint success;
    GLchar infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexSource, NULL);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog
                  << std::endl;
    };

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentSource, NULL);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog
                  << std::endl;
    };

    this->id = createProgram(vertex, fragment);

    if (this->id == 0) {
        std::cout << "FAILED SHADER: " << name << std::endl;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

GLuint Shader::createProgram(GLuint &vertex, GLuint &fragment, GLuint &geometry) {
    GLint success;
    GLchar infoLog[512];
    GLuint program;

    program = glCreateProgram();

    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glAttachShader(program, geometry);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog <<
                  std::endl;

        return 0;
    }

    return program;
}

GLuint Shader::createProgram(GLuint &vertex, GLuint &fragment) {
    GLint success;
    GLchar infoLog[512];
    GLuint program;

    program = glCreateProgram();
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog <<
        std::endl;

        return 0;
    }

    return program;
}

string Shader::loadShader(const string shaderPath) {
    std::string shaderSource;
    std::ifstream shaderStream(shaderPath.c_str(), std::ios::in);

    if (shaderStream.is_open()) {
        std::string line = "";
        while (getline(shaderStream, line)) {
            shaderSource += "\n" + line;
        }
        shaderStream.close();
    } else {
        printf("Impossible to open %s. Are you in the right directory ?\n", shaderPath.c_str());
        getchar();
        return 0;
    }

    return shaderSource;
}

/*void Shader::setupUniform(string uniformName) {
    this->uniformMap.insert(make_pair(uniformName, glGetUniformLocation(this->id, uniformName.c_str())));
}

GLint Shader::getUniformId(string uniformName) {
    return uniformMap[uniformName];
}*/

void Shader::use() {
    glUseProgram(this->id);
}


Shader::~Shader() {
    glDeleteProgram(this->id);
    for (unsigned int i=0; i < uniforms.size(); i++) {
        delete uniforms[i];
        uniforms[i] = 0;
    }
    vector<Uniform*>().swap(uniforms);

}