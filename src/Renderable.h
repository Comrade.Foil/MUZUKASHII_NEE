//
// Created by foil on 17/07/17.
//

#ifndef FIESTA_RENDERABLE_H
#define FIESTA_RENDERABLE_H


#include "Entity.h"
#include "Shader.h"

class Renderable : public Entity{
public:
    Renderable(){};
    virtual ~Renderable(){};

    virtual void render(Shader* program) = 0;
};


#endif //FIESTA_RENDERABLE_H
