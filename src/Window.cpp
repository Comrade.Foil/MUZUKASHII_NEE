//
// Created by foil on 26/07/17.
//

#include <glm/vec2.hpp>
#include "Window.h"

Window::Window() {
    if (!glfwInit()) {
        fprintf(stderr, "ERROR: could not start GLFW3\n");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    windowHandle = glfwCreateWindow(800, 600, "no name", NULL, NULL);

    if (!windowHandle) {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate();
    }

    glfwMakeContextCurrent(windowHandle);

    glewExperimental = GL_TRUE;
    glewInit();

    // get version info
    const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte* version = glGetString(GL_VERSION); // version as a string

    printf("Renderer: %s\n", renderer);
    printf("OpenGL version supported %s\n", version);

    glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetInputMode(windowHandle, GLFW_STICKY_KEYS, GL_TRUE);
}

Window::Window(unsigned int width, unsigned int height, const char* name) {
    if (!glfwInit()) {
        fprintf(stderr, "ERROR: could not start GLFW3\n");
    }

    windowHandle = glfwCreateWindow(width, height, name, NULL, NULL);

    if (!windowHandle) {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate();
    }

    glfwMakeContextCurrent(windowHandle);

    glewExperimental = GL_TRUE;
    glewInit();

    // get version info
    const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte* version = glGetString(GL_VERSION); // version as a string

    printf("Renderer: %s\n", renderer);
    printf("OpenGL version supported %s\n", version);

    glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetInputMode(windowHandle, GLFW_STICKY_KEYS, GL_TRUE);

    glfwSetFramebufferSizeCallback(windowHandle, resize);
}

Window::~Window() {
    glfwTerminate();
    glfwDestroyWindow(windowHandle);
}

void Window::clearColor(float x, float y, float z, float a) {
    this->color = glm::vec4(x, y, z, a);
}

bool Window::shouldClose() {
    if(glfwGetKey(windowHandle, GLFW_KEY_ESCAPE ) == GLFW_PRESS && glfwWindowShouldClose(windowHandle) == 0){
        glfwTerminate();
        return true;
    }
    return false;
}

void Window::clearColor() {
    glClearColor(color.x, color.y, color.z, color.a);
}

void Window::swapBuffers() {
    glfwSwapBuffers(windowHandle);
    glfwPollEvents();
}

void Window::resize(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);

    glfwSetWindowSize(window, width, height);
}

glm::ivec2 Window::getFramebufferSize() {
    glm::ivec2 size;

    glfwGetFramebufferSize(windowHandle, &size.x, &size.y);

    return size;
}

glm::ivec2 Window::getWindowSize() {
    glm::ivec2 size;

    glfwGetWindowSize(windowHandle, &size.x, &size.y);

    return size;
}

GLFWwindow *Window::getWindowHandle() {
    return windowHandle;
}


