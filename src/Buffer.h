//
// Created by foil on 10/08/17.
//

#ifndef MUZUKASHII_NEE_BUFFER_H
#define MUZUKASHII_NEE_BUFFER_H


#include <GL/glew.h>

class Buffer {
private:
    GLuint id;
    bool isActive;
    GLuint index;
    GLenum target;
public:
    ~Buffer() {
        glDeleteBuffers(1, &id);
    }

    Buffer(GLenum target) {
        this->target = target;
        this->isActive = false;

        glGenBuffers(1, &id);
    }

    void setupData(const void *data, int numElements, size_t elementSize, GLenum mode) {
        glBindBuffer(target, id);
        glBufferData(target, numElements * elementSize, data, mode);
        glBindBuffer(target, 0);
    }

    void setupVertexAttribPointer(GLuint index, GLint valuePerVertex, GLenum variableType, GLsizei stride,
                                          int offset) {
        this->index = index;
        glEnableVertexAttribArray(this->index);
        glBindBuffer(this->target, id);
        glVertexAttribPointer(
                this->index,
                valuePerVertex,
                variableType,
                GL_FALSE,           //normalized?
                stride,
                (void *) offset       //buffer offset
        );
    }

    void bind() {
        glBindBuffer(target, id);
    }

    void unbind() {
        glBindBuffer(target, 0);
    }
};


#endif //MUZUKASHII_NEE_BUFFER_H
