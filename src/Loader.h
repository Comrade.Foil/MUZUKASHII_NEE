//
// Created by foil on 10/07/17.
//

#ifndef FIESTA_LOADER_H
#define FIESTA_LOADER_H


#include <string>
#include <iostream>
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "stb_image.h"



#include "global.h"
#include "Utils.h"


using namespace std;

class Loader {
public:
    Loader(std::string, std::string);
    ~Loader();

    bool assimpLoadOBJ(std::string, std::string);
    void loadTexture(string path);


private:
    std::string MODEL_FOLDER_PATH, TEXTURE_FOLDER_PATH;

    void processNode(aiNode *node, const aiScene *scene, MeshData* meshData);
    void processMesh(aiMesh *mesh, const aiScene *scene, MeshData* meshData);
};


#endif //FIESTA_LOADER_H
