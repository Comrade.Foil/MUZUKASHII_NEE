//
// Created by foil on 28/12/16.
//

#ifndef FIESTA_STORAGE_H
#define FIESTA_STORAGE_H


#include <map>
#include <unordered_map>
#include "MeshData.h"
#include "MeshData.h"

class Storage {
private:
    unordered_map<string, MeshData*> meshData;
    unordered_map<string, GLuint> textures;

public:
    Storage();
    ~Storage();

    void storeData(string name, MeshData *mesh);
    //void storeData(string name, Mesh mesh);
    //void storeData(string name, Mesh mesh, vector<GLuint> textures);
    void storeTexture(string name, GLuint texture);

    GLuint getTexture(string name);
    MeshData* getMesh(string name);


};


#endif //FIESTA_STORAGE_H
