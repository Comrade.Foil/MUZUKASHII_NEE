//
// Created by foil on 28/12/16.
//

#include "Storage.h"

Storage::Storage() {
}

void Storage::storeData(string name, MeshData *mesh) {
    this->meshData.insert(make_pair(name, mesh));
}

MeshData* Storage::getMesh(string name) {
    if (meshData.find(name) == meshData.end()) {
        cout << "(Storage) Error: " << name << " is not found in meshData" << endl;
        return meshData[0];
    }

    return meshData[name];
}

void Storage::storeTexture(string name, GLuint texture) {
    this->textures.insert(make_pair(name, texture));
}

GLuint Storage::getTexture(string name) {
    if (textures.find(name) == textures.end()) {
        cout << "(Storage) Error: " << name << " is not found in textures" << endl;
        return textures[0];
    }

    return textures[name];
}

Storage::~Storage() {

}