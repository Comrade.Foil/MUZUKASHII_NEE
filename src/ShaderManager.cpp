//
// Created by foil on 12/12/16.
//

#include "ShaderManager.h"

ShaderManager::ShaderManager(string rootPath) {
    this->ROOT_FOLDER_PATH = rootPath;
};

void ShaderManager::addShader(string name, const string vertexPath, const string fragmentPath, const string geometryPath) {
    string vertexSource = (this->ROOT_FOLDER_PATH + vertexPath);
    string fragmentSource = (this->ROOT_FOLDER_PATH + fragmentPath);
    string geometrySource = (this->ROOT_FOLDER_PATH + geometryPath);

    Shader* sh = new Shader();
    sh->create(name, vertexSource, fragmentSource, geometrySource);

    this->shaderMap.insert(make_pair(name, sh));
}

void ShaderManager::addShader(string name, const string vertexPath, const string fragmentPath) {
    string vertexSource = (this->ROOT_FOLDER_PATH + vertexPath);
    string fragmentSource = (this->ROOT_FOLDER_PATH + fragmentPath);

    Shader* sh = new Shader();
    sh->create(name, vertexSource, fragmentSource);

    this->shaderMap.insert(make_pair(name, sh));
}

void ShaderManager::addShader(Shader* shader) {
    this->shaderMap.insert(make_pair(shader->name, shader));
}

void ShaderManager::addShader(string name, Shader* shader) {
    this->shaderMap.insert(make_pair(name, shader));
}

Shader* ShaderManager::getShader(string name) {
    if (shaderMap.find(name) == shaderMap.end()) {
        cout << "(ShaderManager) Error: shader " << name << " is not found in the map" << endl;
        return shaderMap[0];
    } else {
        return shaderMap[name];
    }
}

void ShaderManager::useShader(string name) {
    if (shaderMap.find(name) == shaderMap.end()) {
        cout << "(ShaderManager) Error: can't use shader " << name << " it is not found in the map" << endl;
    } else {
        shaderMap[name]->use();
    }
}

GLuint ShaderManager::getShaderId(string name) {
    if (shaderMap.find(name) == shaderMap.end()) {
        cout << "(ShaderManager) Error: can't use shader " << name << " it is not found in the map" << endl;
        return 0;
    } else {
        return shaderMap[name]->id;
    }
}

ShaderManager::~ShaderManager() {
    shaderMap.clear();
}



