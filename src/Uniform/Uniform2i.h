//
// Created by foil on 26/07/17.
//

#ifndef SIESTA_UNIFORM2I_H
#define SIESTA_UNIFORM2I_H

#include "Uniform.h"

class Uniform2i : public Uniform {
private:
    int *i1, *i2;
public:
    Uniform2i(){};
    ~Uniform2i(){
        i1 = 0;
        i2 = 0;
    };

    void setId(GLint id) {
        this->variableId = id;
    }

    void setVariable(int *i1, int *i2) {
        this->i1 = i1;
        this->i2 = i2;
    }

    void sendData() {
        glUniform2i(variableId, *i1, *i2);
    }
};
#endif //SIESTA_UNIFORM2I_H
