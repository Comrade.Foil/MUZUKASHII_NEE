//
// Created by foil on 26/07/17.
//

#ifndef SIESTA_UNIFORMMATRIX4FV_H
#define SIESTA_UNIFORMMATRIX4FV_H

#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Uniform.h"

class UniformMatrix4fv : public Uniform {
private:
    glm::mat4 *matrix;

public:
    UniformMatrix4fv(){};
    ~UniformMatrix4fv(){
        matrix = 0;
    };

    void setId(GLint id) {
        this->variableId = id;
    }

    void setVariable(glm::mat4 *matrix) {
        this->matrix = matrix;
    }

    void sendData() {
        glUniformMatrix4fv(variableId, 1, GL_FALSE, glm::value_ptr(*matrix));
    }
};
#endif //SIESTA_UNIFORMMATRIX4FV_H
