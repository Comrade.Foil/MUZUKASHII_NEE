//
// Created by foil on 19/07/17.
//

#ifndef FIESTA_UNIFORM1F_H
#define FIESTA_UNIFORM1F_H

#include "Uniform.h"

class Uniform1f : public Uniform {
private:
    float *f1;
public:
    Uniform1f(){};
    ~Uniform1f(){
        f1 = 0;
    };

    void setId(GLint id) {
        this->variableId = id;
    }

    void setVariable(float *f1) {
        this->f1 = f1;
    }

    void sendData() {
        glUniform1f(variableId, *f1);
    }
};
#endif //FIESTA_UNIFORM1F_H
