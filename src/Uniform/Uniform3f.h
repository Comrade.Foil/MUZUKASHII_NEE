//
// Created by foil on 19/07/17.
//

#ifndef FIESTA_UNIFORM3F_H
#define FIESTA_UNIFORM3F_H

#include <iostream>
#include "Uniform.h"

class Uniform3f : public Uniform {
private:
    float *f1, *f2, *f3;
public:
    Uniform3f(){};
    ~Uniform3f(){
        f1 = 0;
        f2 = 0;
        f3 = 0;
    };

    void setId(GLint id) {
        this->variableId = id;
    }

    void setVariable(float *f1, float *f2, float *f3) {
        this->f1 = f1;
        this->f2 = f2;
        this->f3 = f3;
    }

    void sendData() {
        glUniform3f(variableId, *f1, *f2, *f3);
    }
};

#endif //FIESTA_UNIFORM3F_H
