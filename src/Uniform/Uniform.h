//
// Created by foil on 19/07/17.
//

#ifndef FIESTA_UNIFORMINPUT_H
#define FIESTA_UNIFORMINPUT_H

#include <GL/glew.h>

class Uniform {
protected:
    GLint variableId;
public:
    Uniform(){};
    virtual ~Uniform(){};

    virtual void sendData() = 0;
};
#endif //FIESTA_UNIFORMINPUT_H
