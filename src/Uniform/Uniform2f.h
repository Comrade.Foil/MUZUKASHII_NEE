//
// Created by foil on 19/07/17.
//

#ifndef FIESTA_UNIFORM2F_H
#define FIESTA_UNIFORM2F_H

#include "Uniform.h"

class Uniform2f : public Uniform {
private:
    float *f1, *f2;
public:
    Uniform2f(){};
    ~Uniform2f(){
        f1 = 0;
        f2 = 0;
    };

    void setId(GLint id) {
        this->variableId = id;
    }

    void setVariable(float *f1, float *f2) {
        this->f1 = f1;
        this->f2 = f2;
    }

    void sendData() {
        glUniform2f(variableId, *f1, *f2);
    }
};
#endif //FIESTA_UNIFORM2F_H
