//
// Created by foil on 19/07/17.
//

#include "TRShader.h"

void TRShader::setUniform1f(string uniformName, float *f1) {
    GLint uniformId = glGetUniformLocation(this->id, uniformName.c_str());
    this->uniformMap.insert(make_pair(uniformName, uniformId));

    Uniform1f *u = new Uniform1f();

    u->setId(uniformId);
    u->setVariable(f1);

    uniforms.push_back(u);
}

void TRShader::setUniform2f(string uniformName, float *f1, float *f2) {
    GLint uniformId = glGetUniformLocation(this->id, uniformName.c_str());
    this->uniformMap.insert(make_pair(uniformName, uniformId));

    Uniform2f *u = new Uniform2f();

    u->setId(uniformId);
    u->setVariable(f1, f2);

    uniforms.push_back(u);
}

void TRShader::setUniform2i(string uniformName, int *i1, int *i2) {
    GLint uniformId = glGetUniformLocation(this->id, uniformName.c_str());
    this->uniformMap.insert(make_pair(uniformName, uniformId));

    Uniform2i *u = new Uniform2i();

    u->setId(uniformId);
    u->setVariable(i1, i2);

    uniforms.push_back(u);
}

void TRShader::setUniformMat4fv(string uniformName, glm::mat4 *matrix) {
    GLint uniformId = glGetUniformLocation(this->id, uniformName.c_str());
    this->uniformMap.insert(make_pair(uniformName, uniformId));

    UniformMatrix4fv *u = new UniformMatrix4fv();

    u->setId(uniformId);
    u->setVariable(matrix);

    uniforms.push_back(u);
}

void TRShader::use() {
    glUseProgram(this->id);

    for (int i=0; i < uniforms.size(); i++) {
        uniforms[i]->sendData();
    }
}


