//
// Created by foil on 10/08/17.
//

#ifndef MUZUKASHII_NEE_MESH_H
#define MUZUKASHII_NEE_MESH_H

#include <vector>
#include <GL/glew.h>
#include "Utils.h"

using namespace std;

class MeshData {
public:
    vector<GLfloat> vertexData;
    vector<GLfloat> normalsData;
    vector<GLuint> indices;
    vector<GLfloat> texcoordData;
    vector<GLfloat> colorData;
    vector<string> textures;

    MeshData() {

    }

    ~MeshData() {
        vector<GLfloat>().swap(vertexData);
        vector<GLfloat>().swap(colorData);
        vector<GLfloat>().swap(texcoordData);
        vector<GLuint>().swap(indices);
        vector<string>().swap(textures);
    }

    void pushVertex(vec3 v) {
        vertexData.push_back(v.z);
        vertexData.push_back(v.y);
        vertexData.push_back(v.x);
    }

    void pushNormal(vec3 n) {
        normalsData.push_back(n.z);
        normalsData.push_back(n.y);
        normalsData.push_back(n.x);
    }

    void pushVertexColor(vec3 c) {
        colorData.push_back(c.z);
        colorData.push_back(c.y);
        colorData.push_back(c.x);
    }

    void pushTexcoord(vec2 t) {
        texcoordData.push_back(t.y);
        texcoordData.push_back(t.x);
    }

    void pushIndex(GLuint i) {
        indices.push_back(i);
    }

    void pushTexture(string s) {
        textures.push_back(s);
    }

    GLulong getVerticesNum() {
        return (vertexData.size() / 3);
    }

    GLulong getIndicesNum() {
        return indices.size();
    }

};
#endif //MUZUKASHII_NEE_MESH_H
