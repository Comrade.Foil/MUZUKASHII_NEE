//
// Created by foil on 27/07/17.
//

#ifndef SIESTA_GENERICTEXTURE_H
#define SIESTA_GENERICTEXTURE_H


#include "Framebuffer.h"
#include "ShaderManager.h"

using namespace std;

class GenericTexture {
private:
    Framebuffer fbo;
    Shader* attachedShader;

public:

    GenericTexture(GLuint, GLuint, GLuint, Shader*);
    GenericTexture(GLuint, GLuint, GLuint);

    ~GenericTexture();

    unsigned int getTexture();
    unsigned int getId();
    GLuint getType();

    void attachShader(Shader* shader);
    void update();
    void bindFBO();
    void unbindFBO();

};


#endif //SIESTA_GENERICTEXTURE_H
