//
// Created by stefan on 3/2/18.
//

#ifndef MUZUKASHII_NEE_INPUTHANDLER_H
#define MUZUKASHII_NEE_INPUTHANDLER_H


#include <GLFW/glfw3.h>



class InputHandler {
private:
	GLFWwindow* windowHandle;

public:
	InputHandler();
	~InputHandler();

};


#endif //MUZUKASHII_NEE_INPUTHANDLER_H
