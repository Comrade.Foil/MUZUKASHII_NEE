#version 440 core

layout(location = 0) in vec3 vertexPosition;

uniform vec2 uResolution;
uniform mat4 MVP;
uniform vec3 uPos;

void main() {
    float aspectRatio = uResolution.y/uResolution.x;
    vec3 resPos = vec3(vertexPosition.x + uPos.x, vertexPosition.y + uPos.y, vertexPosition.z + uPos.z);

    gl_Position = MVP * vec4(resPos, 1.0);
}
