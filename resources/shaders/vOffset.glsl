#version 440 core

const float PI = 3.14159265;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform vec2 uResolution;
uniform vec3 uPos;
uniform mat4 MVP;
uniform float uTime;

out vec2 oTexCoord;
out vec3 oNormal;

void main(){
    float offsetx = 2*sin(uTime*1.5);
    float offsety = 2*cos(uTime*1.5);

	float aspectRatio = uResolution.y/uResolution.x;
    vec3 resPos = vec3(vertexPosition.x + uPos.x, vertexPosition.y + uPos.y + offsety, vertexPosition.z + uPos.z);

    gl_Position = MVP * vec4(resPos, 1.0);

	oTexCoord = texCoord;
	oNormal = normal;
}
