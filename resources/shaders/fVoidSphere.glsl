#version 440 core

uniform float uTime;
uniform ivec2 uResolution;

out vec4 output_color;

#define rnd(p) fract(4e4*sin(dot(p,vec2(12.45,57.78))+17.4))

float pulseCircle(vec2 p, float r)
{
    float l = length(p) - r;
    return smoothstep(2.5,3.0,2.*sin(3.5/l + 6.*uTime) + 0.2/l);
}

/*float hash(vec2 co){
    return sin( 2355.44 * fract(sin(dot(co.xy ,vec2(12.9898,78.233)))  * 43758.5453));
}*/

void main()
{
	vec2 uv = gl_FragCoord.xy/uResolution.y;
    vec2 c = vec2(0.5 * uResolution.x/uResolution.y , 0.5);
    vec3 bg = vec3(0.05, 0.05, 0.05);
    vec3 color = vec3(rnd(uv*uTime*0.2));
    //vec3 col = vec3(rnd(uv));

    bg = mix(bg, color, pulseCircle(uv - c, 0.1));

    output_color = vec4(bg, 1.0);
}