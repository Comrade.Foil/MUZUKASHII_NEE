#version 440 core

in vec2 oTexCoord;
in vec4 oNormal;

uniform float uTime;
uniform sampler2D colorTexture;

out vec4 color;

void main(){
    vec3 ligthColor = vec3 (1.0, 1.0, 1.0);
    vec4 lightDirection = vec4(00.0, 0.0, 1.0, 0.0);
    float dirFactor = dot(normalize(oNormal), normalize(lightDirection));
    vec4 dirColor;

    if (dirFactor > 0)
    		dirColor = vec4 ((ligthColor * 0.1 * dirFactor), 1.0f);

    vec4 ambientColor = vec4(0.6, 0.6, 0.6, 1.0);


    vec4 result = texture (colorTexture, oTexCoord) * (dirColor+ambientColor);
    //vec4 result = vec4 (0.5, 0.5, 0.5, 1.0)* (dirColor+ambientColor);

    color = result;
}