#version 440 core

layout(location = 0) in vec3 vertexPosition;
layout(location = 2) in vec2 texCoord;

uniform vec3 uPos;
uniform mat4 MVP;
uniform mat4 uRot;
uniform mat4 uScale;

out vec2 oTexCoord;

void main(){
    vec4 rotatedV = vec4(vertexPosition.xyz, 1.0f) * uRot * uScale;
    vec4 resPos = vec4(uPos.xyz, 0.0f) + rotatedV;//vec4(vertexPosition.x + uPos.x, vertexPosition.y + uPos.y , vertexPosition.z + uPos.z, 1.0f);

    gl_Position = MVP * resPos;

	oTexCoord = texCoord;
}
