#version 440 core

in vec2 oTexCoord;
uniform sampler2D colorTexture;

out vec4 color;

void main(){
    vec4 result = texture (colorTexture, oTexCoord);

    color = result;
}