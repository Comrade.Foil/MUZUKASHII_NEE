#version 440 core

uniform float uTime;
uniform ivec2 uResolution;

out vec4 output_color;

#define rnd(p) fract(4e4*sin(dot(p,vec2(12.45,57.78))+17.4))

/*float hash(vec2 co){
    return sin( 2355.44 * fract(sin(dot(co.xy ,vec2(12.9898,78.233)))  * 43758.5453));
}*/

void main()
{
	vec2 uv = gl_FragCoord.xy/uResolution.y;
    vec3 color = vec3(rnd(uv*uTime*0.2));

    output_color = vec4(color, 1.0);
}