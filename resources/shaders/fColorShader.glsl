#version 440 core

in vec3 oColor;
out vec4 color;

void main() {
    color = vec4(0.5, 0.1, 0.4, 1.0);
}
