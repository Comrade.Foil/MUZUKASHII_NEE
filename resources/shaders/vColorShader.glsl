#version 440 core

layout(location = 0) in vec3 vertexPosition;
//layout(location = 2) in vec3 vertexColor;

uniform vec2 uResolution;
uniform mat4 MVP;

out vec3 oColor;

void main() {
    float aspectRatio = uResolution.y/uResolution.x;

    gl_Position = MVP * vec4(vertexPosition.x, vertexPosition.y, vertexPosition.z, 1.0);
    //oColor = vertexColor;
}
