#define STB_IMAGE_IMPLEMENTATION

#include <iostream>

#include <glm/glm.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include "glm/gtx/quaternion.hpp"

#include "src/global.h"
#include "src/Loader.h"
#include "src/ShaderManager.h"
#include "src/TextureFactory.h"
#include "src/TRShader.h"
#include "src/Camera.h"
#include "src/GameObject.h"

using namespace std;
using namespace glm;

//TODO: shader manager loads shader from JSON file
//TODO: put FBO out from texture factory?
//TODO: make comfortable interface for object transforms
//TODO: camera controls ...
//TODO: Directional light, Point light ...
//TODO: implement filesystem stuff like in LearnOpenGL repo
//TODO: width and height of DynamicTexture FBO must synchronize with initial values and (glViewport)

GLdouble startUpTime = (GLfloat) glfwGetTime();
GLfloat frameTime, deltaTime;
double previousTime = glfwGetTime();

int main() {
	Loader *loader = new Loader("resources/models/", "resources/textures/");
	ShaderManager *ShM = new ShaderManager("resources//shaders//");
	auto *texFac = new TextureFactory();
	
	//g_window->resize(1280, 720);
	g_window->clearColor(0.05, 0.05, 0.05, 1.0);
	
	// tell GL to only draw onto a pixel if the shape is closer to the viewer
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	
	// --------------------------------------- LOAD STUFF ---------------------------------------------------------
	loader->assimpLoadOBJ("cube.obj", "cube");
	loader->assimpLoadOBJ("smooth_cube.obj", "smooth_cube");
	loader->assimpLoadOBJ("sphere.obj", "sphere");
	loader->assimpLoadOBJ("smooth_sphere.obj", "smooth_sphere");
	loader->assimpLoadOBJ("plane.obj", "plane");
	loader->assimpLoadOBJ("planet.obj", "planet");
	loader->assimpLoadOBJ("cyborg/cyborg.obj", "cyborg");
	
	loader->loadTexture("portal.bmp");
	loader->loadTexture("cyborg/cyborg_diffuse.png");
	loader->loadTexture("hat.bmp");
	loader->loadTexture("spheretexture.bmp");
	loader->loadTexture("planet.png");
	// --------------------------------------- LOAD STUFF ---------------------------------------------------------
	
	
	ivec2 WINDOW_SIZE = g_window->getWindowSize();
	ivec2 TEXTURE_SIZE = ivec2(2048, 2048);
	Camera cam(vec3(0.0f, 0.0f, -1.5f), g_window->getWindowHandle());
	
	// -------------------------------------- SHADERS INIT --------------------------------------------------------
	
	
	auto *voidSphereProgram = new TRShader();
	voidSphereProgram->create("voidSphere", "resources//shaders//vEmpty.glsl",
							  "resources//shaders//fVoidSphere.glsl");
	voidSphereProgram->setUniform1f("uTime", &frameTime);
	voidSphereProgram->setUniform2i("uResolution", &TEXTURE_SIZE.x, &TEXTURE_SIZE.y);
	ShM->addShader(voidSphereProgram);
	
	auto *noiseProgram = new TRShader();
	noiseProgram->create("noise", "resources//shaders//vEmpty.glsl", "resources//shaders//fHashNoise.glsl");
	noiseProgram->setUniform1f("uTime", &frameTime);
	noiseProgram->setUniform2i("uResolution", &WINDOW_SIZE.x, &WINDOW_SIZE.y);
	ShM->addShader(noiseProgram);
	
	auto *program = new TRShader();
	program->create("texturing", "resources//shaders//vTexture.glsl",
					"resources//shaders//fTexture.glsl");
	program->setUniform1f("uTime", &frameTime);
	program->setUniform2i("uResolution", &WINDOW_SIZE.x, &WINDOW_SIZE.y);
	program->setUniformMat4fv("MVP", cam.getMVP());
	ShM->addShader(program);
	
	auto *skyboxProgram = new TRShader();
	skyboxProgram->create("skybox", "resources//shaders//vSkybox.glsl",
						  "resources//shaders//fSkybox.glsl");
	skyboxProgram->setUniformMat4fv("MVP", cam.getMVP());
	ShM->addShader(skyboxProgram);
	// -------------------------------------- SHADERS INIT --------------------------------------------------------
	
	
	// ---------------------------------------- FBO's INIT --------------------------------------------------------
	texFac->createDynamicTexture(TEXTURE_SIZE.x, TEXTURE_SIZE.y, GL_COLOR_ATTACHMENT0, voidSphereProgram);
	texFac->createDynamicTexture(TEXTURE_SIZE.x, TEXTURE_SIZE.y, GL_COLOR_ATTACHMENT0, noiseProgram);
	texFac->createDynamicTexture(WINDOW_SIZE.x, WINDOW_SIZE.x, GL_COLOR_ATTACHMENT0,
								 "glowing"); // TODO: this must be just FBO but not DynamicTexture
	// ---------------------------------------- FBO's INIT --------------------------------------------------------
	
	
	// -------------------------------------- INIT OBJECTS -------------------------------------------------------
	GameObject *skyBox = new GameObject("plane", "voidSphere", vec3(0.0, 0.0, 90.0));
	skyBox->transforms.setQuat(3.14 / 2, normalize(vec3(1.0, 0.0, 0.0)));
	skyBox->transforms.setScale(45.0);
	
	GameObject *object2 = new GameObject("cyborg", "cyborg/cyborg_diffuse.png", vec3(1.0, 1.0, 1.0));
	object2->transforms.setPosition(2.0, 1.0, 5.0, 0.0);
	
	
	GameObject *grid = new GameObject("plane", "portal.bmp", vec3(1.0, -1.0, 1.0));
	grid->transforms.setScale(10.0f);
	
	vector<GameObject *> gridArray;
	
	for (int i = 0; i < 10; i++) {
		
		for (int j = 0; j < 10; j++) {
			GameObject *obj = new GameObject("plane", "portal.bmp", vec3(2 * j, -1.0, 2 * i));
			//obj->transforms->setScale(10.0f);
			gridArray.push_back(obj);
		}
		
	}
	
	// -------------------------------------- INIT OBJECTS -------------------------------------------------------
	
	while (!g_window->shouldClose()) {
		g_window->clearColor();
		frameTime = (float) glfwGetTime() - (float) startUpTime;
		
		texFac->updateDynamicTextures();
		
		// --------------------------- RENDER SCENE INTO TEXTURE -------------------------------------------------
		/*texFac->bindSpecificFBO("glowing");
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		cam.update(deltaTime);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, g_storage->getTexture("spheretexture.bmp"));

		ShM->useShader("texturing");

		object->update(frameTime*0.4);
		object->render(ShM->getShaderId("texturing"));
		texFac->unbindSpecificFBO("glowing");*/
		// --------------------------- RENDER SCENE INTO TEXTURE  ------------------------------------------------
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, WINDOW_SIZE.x, WINDOW_SIZE.y);
		
		cam.update(deltaTime);
		
		for (int i = 0; i < gridArray.size(); i++) {
			gridArray[i]->render(ShM->getShader("texturing"));
		}
		
		object2->render(ShM->getShader("texturing"));
		
		skyBox->transforms.setPosition(0.0, 0.0, cam.position.z + 90, 0.0);
		skyBox->render(ShM->getShader("skybox"));
		
		g_window->swapBuffers();
		glfwPollEvents();
		
		deltaTime = glfwGetTime() - frameTime;
		
	}
	
	delete skyBox;
	delete object2;
	delete texFac;
	// close GL context and any other GLFW resources
	
	return 0;
}